import  pandas as pd 
import pdb
import datetime as dt
# importing the required libraries
import gspread
from oauth2client.service_account import ServiceAccountCredentials
import df2gspread as d2g
import logging

logging.basicConfig(filename='run.log' ,level=logging.DEBUG, format='[%(asctime)s][%(levelname)s] %(message)s')


def create_gs_client(keyfile):
    '''
    # create gs API client 
    p1: google json keyfile
    '''
    scope = ['https://spreadsheets.google.com/feeds','https://www.googleapis.com/auth/drive']
    creds = ServiceAccountCredentials.from_json_keyfile_name(keyfile, scope)
    client = gspread.authorize(creds)
    return client 

def read_gs_by_url(gsurl,client):
    '''
    read google sheet by url and return the sheet, 
    only read the first sheet
    # p1: google sheet url 
    # p2: gsclient    
    '''
    # client = create_gs_client(gs_keyfile)
    try:
        sheet = client.open_by_url(gsurl)
        sheet_instance = sheet.get_worksheet(0)
        return sheet,sheet_instance
    except Exception as identifier:
        logging.error(identifier)

def read_google_sheet_url_to_df(url,keyfile):
    # client = create_gs_client(keyfile)
    gs,gs_ist = read_gs_by_url(url,keyfile)
    df = pd.DataFrame.from_dict(gs_ist.get_all_records()) 
    return df

# export the order info from shpify
def export_order_shopify():
    pass

# compare with the exist order, get new order info 
def generate_orderinfo(df_export,df_order):
    # remove the record exist in the "all order"
    # if df_order no contend , copy structure from db_export
    if df_order.shape[0] == 0:
        df_order = pd.DataFrame(columns = ['Name','Created at',\
    'Fulfillment Status','Fulfilled at','Billing Name','Billing Phone','Email','Shipping Name','Total',\
    'product_order_info','Financial Status','Shipping Method','Shipping Phone','Shipping Street',\
        'Shipping Address1','Shipping Address2','Shipping City','Shipping Province','Shipping'])
    df_new_order = pd.DataFrame({})
    for i,v in df_export.iterrows():
        if v['Name'] in df_order['Name'].values:
            df_export = df_export.drop(i)

    # prepare export data add to all order
    # put the product order info into dic and set a colums "product_order_info" to save
    df_export['product_order_info'] = ""
    for i,v in df_export.iterrows():
        # df_export.loc[[i],['product_order_info']] = []
        # df_export.product_order_info[i] = []
        v.product_order_info = []
        dic_prdinfo = {
            "Name" : df_export.loc[i,'Lineitem name'],
            "Quantity" : df_export.loc[i,'Lineitem quantity'], 
            "Price" : df_export.loc[i,'Lineitem price'], 
            "Sku" : df_export.loc[i,'Lineitem sku'],
            "NeedShipping" : df_export.loc[i,'Lineitem requires shipping'] 
        }
        v.product_order_info.append(dic_prdinfo)
        df_export.loc[[i],['product_order_info']] = v.product_order_info

    ## "Fulfillment Status" not empty data 
    df_exp_all_order = df_export[~(df_export['Fulfillment Status'].isnull())][['Name','Created at',\
    'Fulfillment Status','Fulfilled at','Billing Name','Billing Phone','Email','Shipping Name','Total',\
    'product_order_info','Financial Status','Shipping Method','Shipping Phone','Shipping Street', \
        'Shipping Address1','Shipping Address2','Shipping City','Shipping Province','Shipping']]
    # "Fulfillment Status" empty data 
    df_exp_Ful_empty = df_export[df_export['Fulfillment Status'].isnull()][['Name','Created at',\
    'Fulfillment Status','Fulfilled at','Billing Name','Billing Phone','Email','Shipping Name','Total',\
    'product_order_info','Financial Status','Shipping Method','Shipping Phone','Shipping Street',\
        'Shipping Address1','Shipping Address2','Shipping City','Shipping Province','Shipping']]

    # meger record for one order mulitple product 
    for i,v in df_exp_all_order.iterrows():
        if v['Name'] in df_exp_Ful_empty['Name'].values:
            # pdb.set_trace()
            l1 = df_exp_all_order.loc[[i],['product_order_info']].values.tolist()[0]
            l2 = df_exp_Ful_empty[df_exp_Ful_empty['Name'].eq(v['Name'])]['product_order_info'].values.tolist()[0]
            l1.append(l2)
            df_exp_all_order.loc[[i],['product_order_info']] = str(l1)

        # df_exp_Ful_empty[df_exp_Ful_empty['Name'].isin(['#37888','#37884'])]['product_order_info'].values.tolist()
    df_neworder = df_exp_all_order.head(0)
    for i,v in df_exp_all_order.iterrows():
        if v['Name'] not in df_order['Name'].values:
            df_neworder = df_neworder.append(v)

    df_order = df_order.append(df_neworder,ignore_index=True,sort=False)
    logging.info("total " + str(df_neworder.shape[0]) + " rows new order record generated")
    logging.info("total " + str(df_order.shape[0]) + " order record now")
    return df_neworder,df_order


# generate new order status 
def generate_orderstatus(df_order,df_orderstatus):
    if df_orderstatus.shape[0] == 0:
        df_orderstatus = pd.DataFrame(columns = ['ORDER_ID','Order_manage_status', 'Sale_Status',\
            'Store_Status','Deliver_Status','Finance_Status','AfterSale_Status'])
    df_new_orderstatus = df_orderstatus.head(0)
    for i,v in df_order.iterrows():
        if v['Name'] not in df_orderstatus['ORDER_ID'].values:
            df_newrow = pd.DataFrame({
                'ORDER_ID':v['Name'],
                'Order_manage_status':'00000',
                'Sale_Status':'Uninvolved',
                'Store_Status':'Uninvolved',
                'Deliver_Status':'Uninvolved',
                'Finance_Status':'Uninvolved',
                'AfterSale_Status' :'Uninvolved'
            },index=[1])
            df_new_orderstatus = df_new_orderstatus.append(df_newrow,ignore_index=True,sort=False)
    df_orderstatus = df_orderstatus.append(df_new_orderstatus,ignore_index=True,sort=False)
    logging.info("total " + str(df_new_orderstatus.shape[0]) + " rows new orderstatus record generated")
    logging.info("total " + str(df_orderstatus.shape[0]) + " orderstatus record now")    
    return df_new_orderstatus,df_orderstatus


def generate_sale_workorder(df_orderstatus,df_workorder,df_orderinfo):
    if df_workorder.shape[0] == 0:
        df_workorder = pd.DataFrame(columns = ["ORDER_ID","Workorder_ID","Create_Time",\
            "Status","Is_true","Shipping Name","Shipping Phone","Shipping Province",\
            "Shipping Street","Shipping Address1","Shipping Address2","Handler_id",\
                "Handler_name","Handler_time","Remark"])
    # the order not exist in workorder need will generate new
    df_workorder_new = df_workorder.head(0)   
    strdate_yyyymmdd = dt.datetime.now().strftime("%Y%m%d")
    strtime_yyyymmddhhmmss = dt.datetime.now().strftime("%Y/%m/%d %H:%M:%S")
    for i,v in df_orderstatus.iterrows():
        
        if v['ORDER_ID'] not in df_workorder['ORDER_ID'].values:
            df_thisorder = df_orderinfo[df_orderinfo['Name'].str.contains(v['ORDER_ID'])]
            df_newrow = pd.DataFrame({
                "ORDER_ID":df_thisorder['Name'].values[0],
                "Workorder_ID" : strdate_yyyymmdd+'1'+ df_thisorder['Name'].values[0].replace('#',''), 
                "Create_Time": strtime_yyyymmddhhmmss,
                "Status" : 'Processing' , 
                "Is_true" : "Unconfirm",
                "Shipping Name" : df_thisorder['Shipping Name'].values[0] ,
                "Shipping Phone" :  df_thisorder['Shipping Phone'].values[0], 
                "Shipping Province" :  df_thisorder['Shipping Province'].values[0],  
                "Shipping Street" : df_thisorder['Shipping Street'].values[0],  
                "Shipping Address1" :  df_thisorder['Shipping Address1'].values[0],  
                "Shipping Address2" :  df_thisorder['Shipping Address2'].values[0],  
                "Handler_id" : None, 
                "Handler_name" : None,
                "Handler_time" : None ,
                "Remark" : None
                },index=[1])
            df_workorder_new = df_workorder_new.append(df_newrow,ignore_index=True,sort=False)
    df_workorder = df_workorder.append(df_workorder_new,ignore_index=True,sort=False)
    logging.info("total " + str(df_workorder_new.shape[0]) + " rows new sale workorder record generated")
    logging.info("total " + str(df_workorder.shape[0]) + " sale workorder record now")       
    return df_workorder_new,df_workorder
        
# generate store workorder
def generate_store_workorder(df_workorder_sale,df_workorder,df_orderinfo):
    if (df_workorder.shape[0] == 0):
        df_workorder = pd.DataFrame(columns=["ORDER_ID","Workorder_ID",\
            "Create_Time","Status","Is_store_suffice","Pack_Status",\
                "Handler_ID","Handler_Name","Completed_Time","Sale_Remark","Remark"])
    df_workorder_new = df_workorder.head(0)
    strdate_yyyymmdd = dt.datetime.now().strftime("%Y%m%d")
    strtime_yyyymmddhhmmss = dt.datetime.now().strftime("%Y/%m/%d %H:%M:%S")
    for i,v in df_workorder_sale.iterrows():
        if v['ORDER_ID'] not in df_workorder['ORDER_ID'].values and v['Status'] == 'Completed' :
            df_thisorder = df_orderinfo[df_orderinfo['Name'].str.contains(v['ORDER_ID'])]
            df_newrow = pd.DataFrame({
                "ORDER_ID":v['ORDER_ID'],
                "Workorder_ID" : strdate_yyyymmdd+'2'+ v['ORDER_ID'].replace('#',''), 
                "Create_Time": strtime_yyyymmddhhmmss,
                "Status" : 'Processing' , 
                "Is_store_suffice" : "Unconfirm",
                "Pack_Status" : 'Processing' ,
                "Handler_ID" :  None, 
                "Handler_Name" :  None,  
                "Completed_Time" : None,  
                "Sale_Remark" : v['Remark'],  
                "Remark" :  None
                },index=[1])
            df_workorder_new = df_workorder_new.append(df_newrow,ignore_index=True,sort=False)
    df_workorder = df_workorder.append(df_workorder_new,ignore_index=True,sort=False)
    logging.info("total " + str(df_workorder_new.shape[0]) + " rows new store workorder record generated")
    logging.info("total " + str(df_workorder.shape[0]) + " store workorder record now")           
    return df_workorder_new,df_workorder

# generate deliver workorder
def generate_deliver_workorder(df_workorder_store,df_workorder,df_orderinfo):
    if (df_workorder.shape[0] == 0):
        df_workorder = pd.DataFrame(columns=["ORDER_ID","Workorder_ID","Create_Time",\
            "Status","Delivery_Status","Collect_Payment","Shipping_Company",\
                "Shipping_ID","Sent_Time","Arrived_Time","Handler_ID",\
                    "Handler_Name","Completed_Time","Sale_Remark","Store_Remark","Remark"])
    df_workorder_new = df_workorder.head(0)    
    strdate_yyyymmdd = dt.datetime.now().strftime("%Y%m%d")
    strtime_yyyymmddhhmmss = dt.datetime.now().strftime("%Y/%m/%d %H:%M:%S")
    for i,v in df_workorder_store.iterrows():
        if v['ORDER_ID'] not in df_workorder['ORDER_ID'].values and v['Status'] == 'Completed' :
            df_thisorder = df_orderinfo[df_orderinfo['Name'].str.contains(v['ORDER_ID'])]
            collect_payment = 0
            df_newrow = pd.DataFrame({
                "ORDER_ID":v['ORDER_ID'],
                "Workorder_ID" : strdate_yyyymmdd+'2'+ v['ORDER_ID'].replace('#',''), 
                "Create_Time": strtime_yyyymmddhhmmss,
                "Status" : 'Processing' , 
                "Delivery_Status" : "Unconfirm",
                "Collect_Payment" : collect_payment,
                "Shipping_Company" : None ,
                "Shipping_ID" :  None, 
                "Sent_Time" :  None,  
                "Arrived_Time" : None,  
                "Handler_ID" :  None, 
                "Handler_Name" :  None,  
                "Completed_Time" : None,  
                "Sale_Remark" : v['Sale_Remark'],  
                "Store_Remark" : v['Remark'],  
                "Remark" :  None
                },index=[1])
            df_workorder_new = df_workorder_new.append(df_newrow,ignore_index=True,sort=False)
    df_workorder = df_workorder.append(df_workorder_new,ignore_index=True,sort=False)
    logging.info("total " + str(df_workorder_new.shape[0]) + " rows new deliver workorder record generated")
    logging.info("total " + str(df_workorder.shape[0]) + " deliver workorder record now")     
    return df_workorder_new,df_workorder


# generate finance workorder
def generate_finance_workorder(df_workorder_deliver,df_workorder,df_orderinfo):
    if (df_workorder.shape[0] == 0):
        df_workorder = pd.DataFrame(columns=["ORDER_ID","Workorder_ID","Create_Time",\
            "Status","Order_Payment","Reieve_Payment","Handler_ID","Handler_Name",\
                "Completed_Time","Sale_Remark","Store_Remark","Deliver_Remark","Remark"])
    df_workorder_new = df_workorder.head(0)        
    strdate_yyyymmdd = dt.datetime.now().strftime("%Y%m%d")
    strtime_yyyymmddhhmmss = dt.datetime.now().strftime("%Y/%m/%d %H:%M:%S")
    for i,v in df_workorder_deliver.iterrows():
        if v['ORDER_ID'] not in df_workorder['ORDER_ID'].values and v['Status'] == 'Completed' :
            df_thisorder = df_orderinfo[df_orderinfo['Name'].str.contains(v['ORDER_ID'])]
            df_newrow = pd.DataFrame({
                "ORDER_ID":v['ORDER_ID'],
                "Workorder_ID" : strdate_yyyymmdd+'2'+ v['ORDER_ID'].replace('#',''), 
                "Create_Time": strtime_yyyymmddhhmmss,
                "Status" : 'Processing' , 
                "Order_Payment" : df_thisorder['Total'].values[0],
                "Reieve_Payment" : None ,  
                "Handler_ID" :  None, 
                "Handler_Name" :  None,  
                "Completed_Time" : None,  
                "Sale_Remark" : v['Sale_Remark'],  
                "Store_Remark" : v['Store_Remark'],  
                "Deliver_Remark" : v['Remark'],  
                "Remark" :  None
                },index=[1])
            df_workorder_new = df_workorder_new.append(df_newrow,ignore_index=True,sort=False)
    df_workorder = df_workorder.append(df_workorder_new,ignore_index=True,sort=False)
    logging.info("total " + str(df_workorder_new.shape[0]) + " rows new finance workorder record generated")
    logging.info("total " + str(df_workorder.shape[0]) + " finance workorder record now")     
    return df_workorder_new,df_workorder

def generate_aftersale_workorder(df_workorder,df_orderinfo):
    if (df_workorder.shape[0] == 0):
        df_workorder = pd.DataFrame(columns=["ORDER_ID","Workorder_ID","Create_Time",\
        "Status","Sale_Remark","Store_Remark","Deliver_Remark","Finance_Remark","Record"])
    df_workorder_new = df_workorder.head(0)
    return df_workorder_new,df_workorder

def query_workorder_status(ORDER_ID,df_workorderinfo):
    df_thisorder = df_workorderinfo[df_workorderinfo['ORDER_ID'].str.contains(ORDER_ID)]
    status_enum = {
        "Uninvolved" : 0,
        "Processing" : 1, 
        "Completed" : 2 ,
        "Failed" :9
    } 
    status = None 
    if df_thisorder.shape[0] == 0:
        status = 'Uninvolved'
    else:
        status = df_thisorder['Status'].values[0]

    status = [ status_enum[status],status ]
    return status

# update order manage status by workorder status
def update_order_manage_status(df_orderstatus,df_workorder_sale,df_workorder_store,df_workorder_deliver,df_workorder_finance,df_workorder_aftersale):
    for i,v in df_orderstatus.iterrows():
        s1_num,s1_str = query_workorder_status(v['ORDER_ID'],df_workorder_sale)
        s2_num,s2_str = query_workorder_status(v['ORDER_ID'],df_workorder_store)
        s3_num,s3_str = query_workorder_status(v['ORDER_ID'],df_workorder_deliver)
        s4_num,s4_str = query_workorder_status(v['ORDER_ID'],df_workorder_finance)
        s5_num,s5_str = query_workorder_status(v['ORDER_ID'],df_workorder_aftersale)
        manage_status = str(s1_num) + str(s2_num) + str(s3_num) + str(s4_num) + str(s5_num)
        df_newrow = pd.DataFrame({
            'ORDER_ID':v['ORDER_ID'],
            'Order_manage_status' : manage_status,
            'Sale_Status':s1_str,
            'Store_Status': s2_str,
            'Deliver_Status': s3_str,
            'Finance_Status': s4_str,
            'AfterSale_Status':s5_str
        },index=[0]);

        df_orderstatus.loc[i] = df_newrow.loc[0]
    return df_orderstatus
      

def isnert_list_in_gs_head(gs,headlist):
    try:
        gs.insert_row(headlist,index=0)
    except Exception as identifier:
        print(identifier)
        exit
        


def pandas_df_to_list_convert_str(pandas_df):
    '''
    pandas dataframe 
    '''
    pandas_list =  pandas_df.values.tolist()
    len_pandas_list = len(pandas_list)
    for idx_plist in range(len_pandas_list):
        len_pandas_list_list = len(pandas_list[idx_plist])
        for i in range(len_pandas_list_list):
            if type(pandas_list[idx_plist][i]) is not str:
                pandas_list[idx_plist][i] = str(pandas_list[idx_plist][i])

    # col_list = pandas_df.columns.values.tolist() 
    # pandas_list.insert(0,col_list)            
    return pandas_list                

def insert_head_to_gssheet_by_list(headlist,gssheet):
    gssheet.insert_row(headlist,index=0)
    

def append_pandas_to_sheets(pandas_df, gs,gs_sheet, clear = False):
    '''
       
    '''
    if clear:
        gs_sheet.clear()
    try:
        if pandas_df.shape[0] == 0 :
            raise Exception('Empty df no need insert')
        else:
            logging.debug("google sheet " + gs.title +"."+ gs_sheet.title  + " DataFrame append, DataFrame size: " + str(pandas_df.shape))
            pandas_df_list  = pandas_df_to_list_convert_str(pandas_df)
            logging.debug("DataFrame convert to list , list lengh: " + str(len(pandas_df_list)))
            pdb.set_trace()
            gs_sheet.append_rows(pandas_df_list)
    except Exception as identifier:
        print(identifier)
    else:
        logging.info("total "  + str(len(pandas_df_list)) + " record append success")

def update_from_export_excel():
    excel_path_dict = {
        "path_all_order_info" : r'D:\Lihanmin\Project\buy9ja\table_design\All_order_info.xlsx',
        "path_All_order_status" : r'D:\Lihanmin\Project\buy9ja\table_design\All_order_status.xlsx',
        "path_WorkOrder_sale" : r'D:\Lihanmin\Project\buy9ja\table_design\WorkOrder_sale.xlsx',
        "path_WorkOrder_store" : r'D:\Lihanmin\Project\buy9ja\table_design\WorkOrder_store.xlsx',
        "path_WorkOrder_deliver" : r'D:\Lihanmin\Project\buy9ja\table_design\WorkOrder_deliver.xlsx',
        "path_WorkOrder_finance" : r'D:\Lihanmin\Project\buy9ja\table_design\WorkOrder_finance.xlsx',
        "path_WorkOrder_aftersale" : r'D:\Lihanmin\Project\buy9ja\table_design\WorkOrder_aftersale.xlsx'
    }

    try:
        df_export = pd.read_csv(r'D:\Lihanmin\Project\buy9ja\table_design\orders_export.csv')
        df_orderinfo = pd.read_excel(excel_path_dict['path_all_order_info'])
        df_orderstatus = pd.read_excel(excel_path_dict['path_All_order_status'])
        df_workorder_sale = pd.read_excel(excel_path_dict['path_WorkOrder_sale'])
        df_workorder_store = pd.read_excel(excel_path_dict['path_WorkOrder_store'])
        df_workorder_deliver = pd.read_excel(excel_path_dict['path_WorkOrder_deliver'])
        df_workorder_finance = pd.read_excel(excel_path_dict['path_WorkOrder_finance'])
        df_workorder_aftersale = pd.read_excel(excel_path_dict['path_WorkOrder_aftersale'])
    except Exception as identifier:
        print(identifier)
        exit
    logging.info("before change df_export, Total rows:" + str(df_export.shape[0]) + " Total columns:" + str(df_export.shape[1]))
    logging.info("before change df_orderinfo, Total rows:" + str(df_orderinfo.shape[0]) + " Total columns:" + str(df_orderinfo.shape[1]))
    logging.info("before change df_orderstatus, Total rows:" + str(df_orderstatus.shape[0]) + " Total columns:" + str(df_orderstatus.shape[1]))
    logging.info("before change df_workorder_sale, Total rows:" + str(df_workorder_sale.shape[0]) + " Total columns:" + str(df_workorder_sale.shape[1]))
    logging.info("before change df_workorder_store, Total rows:" + str(df_workorder_store.shape[0]) + " Total columns:" + str(df_workorder_store.shape[1]))
    logging.info("before change df_workorder_deliver, Total rows:" + str(df_workorder_deliver.shape[0]) + " Total columns:" + str(df_workorder_deliver.shape[1]))
    logging.info("before change df_workorder_finance, Total rows:" + str(df_workorder_finance.shape[0]) + " Total columns:" + str(df_workorder_finance.shape[1]))
    logging.info("before change df_workorder_aftersale, Total rows:" + str(df_workorder_aftersale.shape[0]) + " Total columns:" + str(df_workorder_aftersale.shape[1]))
             
    # generate new data
    df_orderinfo_new, df_orderinfo = generate_orderinfo(df_export,df_orderinfo)
    df_orderstatus_new, df_orderstatus = generate_orderstatus(df_orderinfo,df_orderstatus)
    df_workorder_sale_new, df_workorder_sale = generate_sale_workorder(df_orderstatus,df_workorder_sale,df_orderinfo)
    df_workorder_store_new, df_workorder_store = generate_store_workorder(df_workorder_sale,df_workorder_store,df_orderinfo)
    df_workorder_deliver_new, df_workorder_deliver = generate_deliver_workorder(df_workorder_store,df_workorder_deliver,df_orderinfo)
    df_workorder_finance_new, df_workorder_finance = generate_finance_workorder(df_workorder_deliver,df_workorder_finance,df_orderinfo)
    df_workorder_aftersale_new, df_workorder_aftersale = generate_aftersale_workorder(df_workorder_aftersale,df_orderinfo)
    df_orderstatus = update_order_manage_status(df_orderstatus,df_workorder_sale,df_workorder_store,df_workorder_deliver,df_workorder_finance,df_workorder_aftersale)
    
    # renew excel content     
    df_orderinfo.to_excel(excel_path_dict['path_all_order_info'],index=False)
    df_orderstatus.to_excel(excel_path_dict['path_All_order_status'],index=False)
    df_workorder_sale.to_excel(excel_path_dict['path_WorkOrder_sale'],index=False)
    df_workorder_store.to_excel(excel_path_dict['path_WorkOrder_store'],index=False)
    df_workorder_deliver.to_excel(excel_path_dict['path_WorkOrder_deliver'],index=False)
    df_workorder_finance.to_excel(excel_path_dict['path_WorkOrder_finance'],index=False)
    df_workorder_aftersale.to_excel(excel_path_dict['path_WorkOrder_aftersale'],index=False)


def update_from_export_gs():
    """
    update google sheeet content by shopify export order info:
    1. filter the valid order info from export.
    2. remove exist order 
    3. add new order record in the order list 
    4. initial order status record and insert to orderstatus sheet
    5. update all order status depend on current workorder status
    6. generate workorder for different team
    """ 
    gs_url_list = {
        "gs_url_all_order_info" : 'https://docs.google.com/spreadsheets/d/1UKyjsNA44jhLucDTXR_ddWf8K48bxDccFuCLF3pbdk0/edit#gid=0',
        "gs_url_All_order_status" : 'https://docs.google.com/spreadsheets/d/1H1Mx0Pwg86-1KfVJl1VSvXcF79k8hati0EdHAonWvHQ/edit#gid=0',
        "gs_url_WorkOrder_sale" : 'https://docs.google.com/spreadsheets/d/1IzVSwSwtgaJDvbDswdKfFcjtAncuojfdvt-avlG7c5g/edit#gid=0',
        "gs_url_WorkOrder_store" : 'https://docs.google.com/spreadsheets/d/1j6LmOGJUcRz36qzD4kzBQAE0vS5E_1RAVJKh7LQ3Tmw/edit#gid=0',
        "gs_url_WorkOrder_deliver" : 'https://docs.google.com/spreadsheets/d/1b9y9mwYJ8JoQUib43UZy6Cf-Hnk7rYkUydELfSam1LQ/edit#gid=0',
        "gs_url_WorkOrder_finance" : 'https://docs.google.com/spreadsheets/d/1EfEY_7k5p6kPF0qSUQE_H1WqHAzYLFloB-NvPy7ITuk/edit#gid=0',
        "gs_url_WorkOrder_aftersale" : 'https://docs.google.com/spreadsheets/d/1ewHRpeJ9JZucIQfLdZNWRZVSWkBFDY6US850x5M3JOw/edit#gid=0'
    }
    gs_keyfile = r'D:\Lihanmin\Project\buy9ja\django-site-1-41e498cb59db.json'
    client = create_gs_client(gs_keyfile)

    # read export order csv file
    try:
        df_export = pd.read_csv(r'D:\Lihanmin\Project\buy9ja\table_design\orders_export.csv')
    except Exception as identifier:
        print(identifier)
    
    # read google sheet to sheet
    gs_order_info,gs_order_info_sheet = read_gs_by_url(gs_url_list['gs_url_all_order_info'],client)
    gs_order_status,gs_order_status_sheet = read_gs_by_url(gs_url_list['gs_url_All_order_status'],client)
    gs_workorder_sale,gs_workorder_sale_sheet = read_gs_by_url(gs_url_list['gs_url_WorkOrder_sale'],client)
    gs_workorder_store,gs_workorder_store_sheet = read_gs_by_url(gs_url_list['gs_url_WorkOrder_store'],client)
    gs_workorder_deliver,gs_workorder_deliver_sheet = read_gs_by_url(gs_url_list['gs_url_WorkOrder_deliver'],client)
    gs_workorder_finance,gs_workorder_finance_sheet = read_gs_by_url(gs_url_list['gs_url_WorkOrder_finance'],client)
    gs_workorder_aftersale,gs_workorder_aftersale_sheet = read_gs_by_url(gs_url_list['gs_url_WorkOrder_aftersale'],client)
    # convert sheet to DataFrame
    df_orderinfo = pd.DataFrame.from_dict(gs_order_info_sheet.get_all_records())
    df_orderstatus = pd.DataFrame.from_dict(gs_order_status_sheet.get_all_records())
    df_workorder_sale = pd.DataFrame.from_dict(gs_workorder_sale_sheet.get_all_records())
    df_workorder_store = pd.DataFrame.from_dict(gs_workorder_store_sheet.get_all_records())
    df_workorder_deliver = pd.DataFrame.from_dict(gs_workorder_deliver_sheet.get_all_records())
    df_workorder_finance = pd.DataFrame.from_dict(gs_workorder_finance_sheet.get_all_records())
    df_workorder_aftersale = pd.DataFrame.from_dict(gs_workorder_aftersale_sheet.get_all_records())
    logging.info("before change df_export, Total rows:" + str(df_export.shape[0]) + " Total columns:" + str(df_export.shape[1]))
    logging.info("before change df_orderinfo, Total rows:" + str(df_orderinfo.shape[0]) + " Total columns:" + str(df_orderinfo.shape[1]))
    logging.info("before change df_orderstatus, Total rows:" + str(df_orderstatus.shape[0]) + " Total columns:" + str(df_orderstatus.shape[1]))
    logging.info("before change df_workorder_sale, Total rows:" + str(df_workorder_sale.shape[0]) + " Total columns:" + str(df_workorder_sale.shape[1]))
    logging.info("before change df_workorder_store, Total rows:" + str(df_workorder_store.shape[0]) + " Total columns:" + str(df_workorder_store.shape[1]))
    logging.info("before change df_workorder_deliver, Total rows:" + str(df_workorder_deliver.shape[0]) + " Total columns:" + str(df_workorder_deliver.shape[1]))
    logging.info("before change df_workorder_finance, Total rows:" + str(df_workorder_finance.shape[0]) + " Total columns:" + str(df_workorder_finance.shape[1]))
    logging.info("before change df_workorder_aftersale, Total rows:" + str(df_workorder_aftersale.shape[0]) + " Total columns:" + str(df_workorder_aftersale.shape[1]))
    
    cols_df_orderinfo  = ['Name','Created at',\
    'Fulfillment Status','Fulfilled at','Billing Name','Billing Phone','Email','Shipping Name','Total',\
    'product_order_info','Financial Status','Shipping Method','Shipping Phone','Shipping Street',\
        'Shipping Address1','Shipping Address2','Shipping City','Shipping Province','Shipping']
    cols_df_orderstatus = ['ORDER_ID','Order_manage_status', 'Sale_Status',\
            'Store_Status','Deliver_Status','Finance_Status','AfterSale_Status']
    cols_df_workorder_sale = ["ORDER_ID","Workorder_ID","Create_Time",\
            "Status","Is_true","Shipping Name","Shipping Phone","Shipping Province",\
            "Shipping Street","Shipping Address1","Shipping Address2","Handler_id",\
                "Handler_name","Handler_time","Remark"]
    cols_df_workorder_store = ["ORDER_ID","Workorder_ID",\
            "Create_Time","Status","Is_store_suffice","Pack_Status",\
                "Handler_ID","Handler_Name","Completed_Time","Sale_Remark","Remark"]                
    cols_df_workorder_deliver = ["ORDER_ID","Workorder_ID","Create_Time",\
            "Status","Delivery_Status","Collect_Payment","Shipping_Company",\
                "Shipping_ID","Sent_Time","Arrived_Time","Handler_ID",\
                    "Handler_Name","Completed_Time","Sale_Remark","Store_Remark","Remark"]
    cols_df_workorder_finance = ["ORDER_ID","Workorder_ID","Create_Time",\
            "Status","Order_Payment","Reieve_Payment","Handler_ID","Handler_Name",\
                "Completed_Time","Sale_Remark","Store_Remark","Deliver_Remark","Remark"]  
    cols_df_workorder_aftersale = ["ORDER_ID","Workorder_ID","Create_Time",\
        "Status","Sale_Remark","Store_Remark","Deliver_Remark","Finance_Remark","Record"] 

    try:
        if len(gs_order_info_sheet.get_all_values()) == 0:  gs_order_info_sheet.insert_row(cols_df_orderinfo,index=1)
        if len(gs_order_status_sheet.get_all_values()) == 0:  gs_order_status_sheet.insert_row(cols_df_orderstatus,index=1)
        if len(gs_workorder_sale_sheet.get_all_values()) == 0:  gs_workorder_sale_sheet.insert_row(cols_df_workorder_sale,index=1)
        if len(gs_workorder_store_sheet.get_all_values()) == 0:  gs_workorder_store.i_sheetnsert_row(cols_df_workorder_store,index=1)
        if len(gs_workorder_deliver_sheet.get_all_values()) == 0:  gs_workorder_deliver_sheet.insert_row(cols_df_workorder_deliver,index=1)
        if len(gs_workorder_finance_sheet.get_all_values()) == 0:  gs_workorder_finance.ins_sheetert_row(cols_df_workorder_finance,index=1)
        if len(gs_workorder_aftersale_sheet.get_all_values()) == 0:  gs_workorder_aftersale_sheet.insert_row(cols_df_workorder_aftersale,index=1)
    except Exception as identifi_sheeter:
        print(identifier)

    # generate new data
    df_orderinfo_new, df_orderinfo = generate_orderinfo(df_export,df_orderinfo)
    df_orderstatus_new, df_orderstatus = generate_orderstatus(df_orderinfo,df_orderstatus)
    df_workorder_sale_new, df_workorder_sale = generate_sale_workorder(df_orderstatus,df_workorder_sale,df_orderinfo)
    df_workorder_store_new, df_workorder_store = generate_store_workorder(df_workorder_sale,df_workorder_store,df_orderinfo)
    df_workorder_deliver_new, df_workorder_deliver = generate_deliver_workorder(df_workorder_store,df_workorder_deliver,df_orderinfo)
    df_workorder_finance_new, df_workorder_finance = generate_finance_workorder(df_workorder_deliver,df_workorder_finance,df_orderinfo)
    df_workorder_aftersale_new, df_workorder_aftersale = generate_aftersale_workorder(df_workorder_aftersale,df_orderinfo)
    
    # renew google sheet online content
    append_pandas_to_sheets(df_orderinfo_new,gs_order_info,gs_order_info_sheet,clear = False)
    # append_pandas_to_sheets(df_orderstatus_new,gs_order_status,gs_order_status_sheet,clear = False)
    append_pandas_to_sheets(df_workorder_sale_new,gs_workorder_sale,gs_workorder_sale_sheet,clear = False)
    append_pandas_to_sheets(df_workorder_store_new,gs_workorder_store,gs_workorder_store_sheet,clear = False)
    append_pandas_to_sheets(df_workorder_deliver_new,gs_workorder_deliver,gs_workorder_deliver_sheet,clear = False)
    append_pandas_to_sheets(df_workorder_aftersale_new,gs_workorder_aftersale,gs_workorder_aftersale_sheet,clear = False)

    df_orderstatus = update_order_manage_status(df_orderstatus,df_workorder_sale,df_workorder_store,df_workorder_deliver,df_workorder_finance,df_workorder_aftersale)   
    gs_order_status_sheet.clear()
    gs_order_status_sheet.insert_row(cols_df_orderstatus,index=1)
    append_pandas_to_sheets(df_orderstatus_new,gs_order_status,gs_order_status_sheet,clear = False)

    # append_pandas_to_sheets(df_orderinfo,gs_order_info,clear = False)
    # append_pandas_to_sheets(df_orderstatus,gs_order_status,clear = False)
    # append_pandas_to_sheets(df_workorder_sale,gs_workorder_sale,clear = False)
    # append_pandas_to_sheets(df_workorder_store,gs_workorder_store,clear = False)
    # append_pandas_to_sheets(df_workorder_deliver,gs_workorder_deliver,clear = False)
    # append_pandas_to_sheets(df_workorder_finance,gs_workorder_aftersale,clear = False)
    # append_pandas_to_sheets(df_workorder_aftersale,gs_workorder_aftersale,clear = False)

# main
if __name__ == "__main__":
    update_from_export_gs()
    # update_from_export_excel()
    # gs_url_All_order_status = 'https://docs.google.com/spreadsheets/d/1H1Mx0Pwg86-1KfVJl1VSvXcF79k8hati0EdHAonWvHQ/edit#gid=0'
    # scope = ['https://spreadsheets.google.com/feeds','https://www.googleapis.com/auth/drive']
    # gs_keyfile = r'D:\Lihanmin\Project\buy9ja\django-site-1-41e498cb59db.json'
    # creds = ServiceAccountCredentials.from_json_keyfile_name(gs_keyfile, scope)
    # client = gspread.authorize(creds)
    # sheet = client.open_by_url(gs_url_All_order_status)
    # sheet_instance = sheet.get_worksheet(0)

